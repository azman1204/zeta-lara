CREATE TABLE post (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  user_id INT NOT NULL,
  content TEXT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id)
);

INSERT INTO post(user_id, content) VALUES(1, 'test 1');
INSERT INTO post(user_id, content) VALUES(1, 'test 2');
INSERT INTO post(user_id, content) VALUES(2, 'test 3');
INSERT INTO post(user_id, content) VALUES(2, 'test 4');

SELECT * FROM user;
SELECT * FROM post;