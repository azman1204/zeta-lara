<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller {
    // show login page
    function login() {
        echo Hash::make('1234');
        return view('login/form');
    }

    // authentication
    function auth(Request $req) {
        $user_id = $req->user_id;
        $password = $req->password;
        echo "$user_id $password";
        $credentials = ['user_id' => $user_id, 'password' => $password];
        if (Auth::attempt($credentials)) {
            // berjaya
            return redirect('/employee/index');
        } else {
            // gagal
            // note: with() - set flash session
            return redirect('/login')->with('err', 'Authentication Failed');
        }
    }

    // logout
    function logout() {
        Auth::logout();
        session()->invalidate(); // kill semua session
        return redirect('/login');
    }
}
