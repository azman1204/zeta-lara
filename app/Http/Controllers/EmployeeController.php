<?php
namespace App\Http\Controllers;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    // list all employee
    public function index()
    {
        // ini return array of obj. Employee
        $result = Employee::all(); // select * from employee
        return view('employee/index', ['rows' => $result]);
    }

    // show new employee form
    public function create() {
        $emp = new Employee();
        return view('employee/form', ['emp' => $emp]);
    }

    // insert dan update data
    public function save(Request $req) {
        //dd($req->all()); // die dump
        $id = $req->id;
        if (empty($id)) {
            // insert
            $emp = new Employee();
        } else {
            // update
            $emp = Employee::find($id);
        }

        $emp->name   = $req->name;
        $emp->dob    = $req->dob;
        $emp->salary = $req->salary;
        $emp->save();
        echo 'data telah disimpan';
    }

    // show ori data
    public function edit($id) {
        $emp = Employee::find($id); // find return an object (Employee model)
        return view('employee/form', ['emp' => $emp]);
    }

    public function delete($id) {
        Employee::find($id)->delete();
        return redirect('/employee/index');
    }
}

// Asas Laravel MVC + R
// Asas Sistem CRUD = Create, Retrieve, Update, Delete
