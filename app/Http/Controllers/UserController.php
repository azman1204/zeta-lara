<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {
    // GET /user
    public function index() {
        $rows = User::all();
        return view('user/index', ['rows' => $rows]);
    }

    // GET /user/create
    public function create() {
        $user = new User();
        return view('user/form', ['user' => $user]);
    }

    // POST /user
    public function store(Request $request) {
        $user = new User();
        $user->name = $request->name;
        $user->user_id = $request->user_id;
        $user->password = $request->password;

        $rules = $user->getRules();
        $message = $user->getMessage();
        // jika validation gagl, auto redirect ke prev form
        $request->validate($rules, $message);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect('/user');
        echo "store";
    }

    // GET user/{id}
    public function show($id)
    {
        echo "show";
    }

    // GET user/{id}/edit
    public function edit($id) {
        $user = User::find($id);
        return view('user/form', ['user' => $user]);
    }

    // PUT OR PATCH /user
    public function update(Request $request, $id) {
        $user = User::find($id);
        $rules = $user->getRules();

        // check samada user tukar password
        if ($request->password == $user->password) {
            //tak tukar password
            unset($rules['password']);
        } else {
            // change password
            $user->password = Hash::make($request->password);
        }

        unset($rules['user_id']); // remove satu value dr array
        $message = $user->getMessage();
        $request->validate($rules, $message);
        $user->name = $request->name;
        $user->user_id = $request->user_id;
        $user->save();
        return redirect('/user');
    }

    // DESTROY /user/{id}
    public function destroy($id) {
        User::find($id)->delete();
        return redirect('/user');
    }
}
