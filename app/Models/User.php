<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    // Prefer Convention over Configuration
    // note : jika nama table users, tak perlu ada line di bawah
    protected $table = 'user';

    // tida column created_at dan updated_At
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // return array of form validation rules
    public function getRules() {
        return [
            'name'     => 'required', // name tidak boleh empty
            'password' => 'min:5|max:20',
            'user_id'  => 'required|unique:user' // unique - x boleh duplicate user_id dlm table user
        ];
    }

    public function getMessage() {
        return [
            'name.required' => 'Nama wajib diisi',
            'password.min'  => 'Minima katalaluan mestilah 5 karakter',
            'password.max'  => 'Maksima katalaluan ialah 20 karakter',
            'user_id.unique'=> 'Id Pengguna ini sudah wujud',
            'user_id.required'=> 'Id Pengguna wajib diisi',
        ];
    }
}
