<?php
use App\Http\Controllers\HelloController;
use Illuminate\Support\Facades\Route;

include 'sample.php';

// http://zeta_lara.test
Route::get('/', function () {
    return view('welcome'); // rsources/views/welcome.blade.php
});

// http://zeta_lara.test/hello
// note: param1 -> pattern URL, param2 -> anonymous function yg terus execute
Route::get('/hello10', function() {
    echo 'Hello Laravel';
})->name('first');

Route::get('/hello.html', function() {
    return view('hello', ['name' => 'Azman']); // resources/views/hello.blade.php
})->name('contact');

Route::get('/brain', [HelloController::class, 'index']);
Route::get('/greeting', [HelloController::class, 'greeting'])
->name('greeting');
// shift + alt + down arrow = duplicate

Route::get('/brain2', function() {
    return redirect('/brain');
});

// redirect using route name
Route::get('/greeting2', function() {
    return redirect()->route('greeting');
});
