<?php
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;

Route::middleware(['testing', 'pak-guard'])->group(function() {
    Route::get('/employee/index', [EmployeeController::class, 'index']);
    Route::get('/employee/create', [EmployeeController::class, 'create']);
    Route::post('/employee/save', [EmployeeController::class, 'save']);
    Route::get('/employee/edit/{id}', [EmployeeController::class, 'edit']);
    Route::get('/employee/delete/{id}', [EmployeeController::class, 'delete']);
});

// RESTful controller
Route::resource('user', UserController::class)->middleware('pak-guard');

// Login
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login', [LoginController::class, 'auth']);
Route::get('/logout', [LoginController::class, 'logout']);
