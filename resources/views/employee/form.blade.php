@extends('master')
@section('content')

<form action="/employee/save" method="post">
    @csrf
    <input hidden name="id" value="{{ $emp->id }}">
    <div class="mb-3">
        <label class="form-label">Name</label>
        <input value="{{ $emp->name }}" type="text" name="name" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label">Date of Birth</label>
        <input value="{{ $emp->dob }}" type="date" name="dob" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label">Salary</label>
        <input value="{{ $emp->salary }}" step="any" type="number" name="salary" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label"></label>
        <input type="submit" class="btn btn-primary">
    </div>
</form>

@endsection
