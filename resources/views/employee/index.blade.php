@extends('master')
@section('content')

    <h4>Employee List</h4>

    <a href="/employee/create" class="btn btn-primary mb-2">New Employee</a>

    <table class="table table-striped table-hover table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Tarikh Lahir</th>
            <th>Gaji</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
            @foreach($rows as $emp)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $emp->name }}</td>
                <td>{{ date('d/m/Y', strtotime($emp->dob))}}</td>
                <td>{{ $emp->salary}}</td>
                <td>
                    <a href="/employee/edit/{{ $emp->id }}"
                    class="btn btn-outline-primary">Edit</a>

                    <a href="/employee/delete/{{ $emp->id }}"
                    class="btn btn-outline-danger"
                    onclick="return confirm('Anda Pasti ?')">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
