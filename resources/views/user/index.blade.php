@extends('master')
@section('content')
    <h4>User List</h4>
    <a href="/user/create" class="btn btn-primary mb-2">New User</a>
    <table class="table table-striped table-hover table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>User ID</th>
            <th style="width:15%">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach($rows as $user)
            <tr>
                <td scope="row">{{ $loop->iteration }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->user_id}}</td>
                <td align="center">
                    <a href="/user/{{ $user->id }}/edit"
                    class="btn btn-outline-primary">Edit</a>

                    <form action="/user/{{ $user->id }}" method="post" style="float:left;">
                        @method('delete')
                        @csrf
                        <input type="submit" value="Delete" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
