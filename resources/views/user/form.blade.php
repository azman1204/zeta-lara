@extends('master')
@section('content')

    @if (empty($user->id))
        <form action="/user" method="post">
    @else
        <form action="/user/{{ $user->id }}" method="post">
        @method('PATCH')
    @endif

    @csrf

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $err)
                <li>{{ $err }}</li>
            @endforeach
        </div>
    @endif

    <input hidden name="id" value="{{ $user->id }}">
    <div class="mb-3">
        <label class="form-label">Name</label>
        <input value="{{ old('name', $user->name) }}" type="text" name="name" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label">User ID</label>
        <input value="{{ old('user_id',$user->user_id) }}" type="text" name="user_id" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label">Password</label>
        <input value="{{ $user->password }}" type="password" name="password" class="form-control">
    </div>

    <div class="mb-3">
        <label class="form-label"></label>
        <input type="submit" class="btn btn-primary">
    </div>
</form>

@endsection
