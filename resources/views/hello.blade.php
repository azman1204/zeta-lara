@extends('master')
@section('content')
<h4>Hello From Blade</h4>
{{ $name }}
<a href="{{ route('first') }}">Hello</a>
<a href="/hello">Hello 2</a>

@foreach([1,2,3,4,5,6] as $name)
    {{ $name }} <br>
@endforeach

@if ($name == 'azman')
    Hello Azman
@else
    Hello Unknown
@endif

<?php
if ($name == 'azman') {
?>
    Hello Azman
<?php
} else {
?>
    Hello Unknown
<?php
}
?>
@endsection
